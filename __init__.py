"""Basic interaction with RN2483"""
import time
import logging
from serial import Serial


logger = logging.getLogger('LoraRN2483')


RADIO_FREQ = 869000000  # 869MHz - legal in EU
RADIO_PWR = 8           # 0.099W (see RN2483 datasheet table 2-5) (default 1)
RADIO_SF = 'sf7'        # Spreading factor - sf7 (default sf120
RADIO_FREQ_CORR = 50    # Automatic freq correction (kHz) (default 41.7)
RADIO_FREQ_BAND = 100   # Signal bandwidth (kHz) (default 25)
RADIO_PREAMBLE = 8      # Preamble length (default 8)
RADIO_CRC = 'on'        # CRC enabled
RADIO_IQI = 'off'       # Invert IQ
RADIO_CR = '4/7'        # Coding rate
RADIO_WD = 15000        # Watchdog timer
RADIO_SYNC = 34         # Sync word
RADIO_BW = 125          # Operating bandwidth


LORA_UART_BAUD = 57600
LORA_UART_DEV = '/dev/ttyACM0'
LORA_UART_TIMEOUT = 10


LORA_OK = b'ok'
LORA_INVALID = b'invalid_param'


LORA_P2P_INIT_SEQUENCE = (
    ('mac pause',),                         # pause LoRaWAN
    ('radio set mod lora',),                # set radio to lora mode
    ('radio set freq', RADIO_FREQ),         # set radio freq
    ('radio set pwr', RADIO_PWR),           # set radio power
    ('radio set sf', RADIO_SF),             # set spreading factor
    ('radio set afcbw', RADIO_FREQ_CORR),   # set auto freq correction
    ('radio set rxbw', RADIO_FREQ_BAND),    # set signal bandwidth
    ('radio set prlen', RADIO_PREAMBLE),    # set preamble length
    ('radio set crc', RADIO_CRC),           # set CRC
    ('radio set iqi', RADIO_IQI),           # set IQI
    ('radio set cr', RADIO_CR),             # set codding rate
    ('radio set wdt', RADIO_WD),            # set watchdog
    ('radio set sync', RADIO_SYNC),         # set sync word
    ('radio set bw', RADIO_BW),             # set operating bandwidth
)


class LoraRN2483(Serial):
    """Simpilfied lora rn2483 interraction through serial port."""
    # default serial connection parameters

    def __init__(self, **kwargs):
        kwargs.setdefault('port', LORA_UART_DEV)
        kwargs.setdefault('baudrate', LORA_UART_BAUD)
        kwargs.setdefault('timeout', LORA_UART_TIMEOUT)
        logger.debug('Initiating with %s', kwargs)
        super().__init__(**kwargs)

    def send_command(self, command, *args, read_response=True):
        """Sends ``command`` with optional ``args``
        and returns lora's respnose if ``read_response`` is set,
        otherwise returns None.
        """
        params = list(args)
        params.insert(0, command)
        cmd = b' '.join(str(x).encode() for x in params)
        logger.debug('Sending command %s', cmd)
        cmd = b''.join([cmd, b'\r\n'])
        self.write(cmd)

        if not read_response:
            return None

        def _results():
            """Wait for data to apear on serial and then returns
            all the lines.
            This way data is returned as soon as it shows up, in
            case of using ``readlines`` or ``readall`` it would
            take at least ``self.timeout`` seconds.
            """
            while not self.in_waiting:
                pass
            while self.in_waiting:
                yield self.readline().strip()

        return tuple(_results())

    def init_p2p(self):
        """Initialize radio for p2p operation"""
        logger.debug('Initializing P2P')
        for cmd, *args in LORA_P2P_INIT_SEQUENCE:
            resp = self.send_command(cmd, *args)
            time.sleep(10e-3)
            logger.debug('Response %s', resp)
