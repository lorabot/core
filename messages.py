"""LoRa messages"""
FWD = 0
REV = 1
LEFT = 2
RIGHT = 3

HALT = '00'
DRIVE = '01'
STOP = '02'
TURN = '03'


def halt():
    """Returns halt request"""
    return HALT


def drive(direction, speed, step, del_ms=None):
    """Returns drive request with given parameters"""
    dir_bit = (1 if direction == FWD else 0) << 7
    dir_speed = hex(dir_bit | speed)[2:]
    del_ms = hex(del_ms)[2:] if del_ms else ''
    return '{}{}{}{}'.format(DRIVE, dir_speed, hex(step)[2:], del_ms)


def stop(step, del_ms=None):
    """Returns stop request with provided parameters"""
    del_ms = hex(del_ms)[2:] if del_ms else ''
    return '{}{}{}'.format(STOP, hex(step)[2:], del_ms)


def turn(side, strength, step, del_ms=None):
    """Returns turn request with provided parameters"""
    side_bit = (1 if side == RIGHT else 0) << 7
    side_strength = hex(side_bit | step)[2:]
    del_ms = hex(del_ms)[2:] if del_ms else ''
    return '{}{}{}{}'.format(TURN, side_strength, hex(step)[2:], del_ms)
